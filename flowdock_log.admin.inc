<?php
/**
 * @file
 * Contains the admin forms for Flowdock Log.
 */

module_load_include('inc', 'flowdock_log', 'flowdock_log_drupal_api.class');

/**
 * Implements hook_form().
 *
 * System settings admin form for Flowdock Log
 */
function flowdock_log_admin_form($form, &$form_state) {
  $form = array();
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#description' => t('General for the Flowdock API.'),
  );

  $fd_token = variable_get('flowdock_log_flow_api_token');

  $form['general']['flowdock_log_flow_api_token'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Flow API Token'),
    '#description' => t('To get a token please visit the @link page. You will need to login to get a token. Copy the string in the "Flow API tokens" section after choosing your flow where you want the logs to appear.', array(
      '@link' => l(t('Flowdock Tokens'), 'https://flowdock.com/account/tokens', array(
        'external' => TRUE,
        'attributes' => array('target' => '_blank'),
    )))),
    '#default_value' => $fd_token,
  );
  $form['general']['flowdock_log_from_address'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('From Address'),
    '#description' => t("The email address where you would like messages to be from in your flow's team inbox."),
    '#default_value' => variable_get('flowdock_log_from_address'),
  );
  $form['general']['flowdock_log_source'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Source'),
    '#description' => t('The source which should appear for the message in your flow team inbox.'),
    '#default_value' => variable_get('flowdock_log_source'),
  );
  $form['general']['flowdock_log_subject'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Subject'),
    '#description' => t('The subject which will appear in your team inbox.'),
    '#default_value' => variable_get('flowdock_log_subject'),
  );
  $form['testing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tests'),
  );
  if ($fd_token) {
    $form['testing']['message'] = array(
      '#type' => 'markup',
      '#markup' => l(t('Create a test message in the team inbox'), 'admin/flowdock/log/test/message/create'),
    );
  }
  else {
    $form['testing']['message'] = array(
      '#type' => 'markup',
      '#markup' => t('Tests are not available until you complete the fields above.'),
    );
  }
  return system_settings_form($form);
}


/**
 * Implements hook_form().
 *
 * Admin confirmation form to send a test message to a flow.
 */
function flowdock_log_message_test_form($form, &$form_state) {
  return confirm_form($form, t('Are you sure you want to create a test message in the team box?'), 'admin/config/flowdock/log', t('You can delete this message by visiting the team inbox in your flow later.'), t('Create'), t('Cancel'));
}

/**
 * Implements hook_form_submit().
 */
function flowdock_log_message_test_form_submit($form, &$form_state) {
  $fd = new FlowdockLogDrupalApi();
  try {
    $fd->createDrupalTeamInboxMessage('This message was created using Drupal. Your test was successful.', array(
      'drupal',
      'test',
    ));
    drupal_set_message('Created a test message in your team inbox.');
  }
  catch(Exception $e) {
    drupal_set_message('A test message was not able to be created. ' . $e->getMessage(), 'error');
  }
  $form_state['redirect'] = 'admin/config/flowdock/log';
}
