<?php
/**
 * @file
 * The Flowdock message format for the watchdog log.
 *
 * @see template_preprocess_flowdock_log_message_format()
 *
 * @ingroup themeable
 */
?>
<table>
  <tr>
    <th style="text-align: left;">Field</th>
    <th style="text-align: left;">Value</th>
  </tr>
  <tr>
    <td>message:</td>
    <td>!message</td>
  </tr>
  <tr>
    <td>timestamp:</td>
    <td>!timestamp</td>
  </tr>
  <tr>
    <td>base_url:</td>
    <td>!base_url</td>
  </tr>
  <tr>
    <td>request_uri:</td>
    <td>!request_uri</td>
  </tr>
  <tr>
    <td>referer:</td>
    <td>!referer</td>
  </tr>
  <tr>
    <td>uid:</td>
    <td>!uid</td>
  </tr>
  <tr>
    <td>link:</td>
    <td>!link</td>
  </tr>
</table>
