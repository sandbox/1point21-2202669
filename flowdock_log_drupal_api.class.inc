<?php
/**
 * @file
 * Contains \FlowdockLog\DrupalApi
 * 
 * This class will be used to implement the FlowdockApiCurl class
 * using Drupal functions.
 */

/**
 * The class sets up Flowdock API CURL class using Drupal functions.
 */
class FlowdockLogDrupalApi extends FlowdockApiCurl {
  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $flow_api_token = variable_get('flowdock_log_flow_api_token', NULL);
    parent::__construct($flow_api_token);
  }

  /**
   * {@inheritdoc}
   */
  public function createDrupalTeamInboxMessage($content, $tags = NULL) {
    $source = variable_get('flowdock_log_source', NULL);
    $from_address = variable_get('flowdock_log_from_address', NULL);
    $subject = variable_get('flowdock_log_subject', NULL);
    $message = new FlowdockApiTeamInboxMessage($source, $from_address, $subject, $content, $tags);
    return parent::createTeamInboxMessage($message);
  }
}
