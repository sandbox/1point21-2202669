The Flowdock Log module will place all of your Drupal watchdog log 
messages in the Team Inbox of a Flowdock Flow which you select. This 
will allow your Dev team to see all of you Drupal log messages in a 
single location.  Log message severity, ip address, module name, and a 
general log / drupal tag are added to each log message which 
allow quick and easy filtering in Flowdock.

This modules uses Drupal's core queue system so your Drupal visitors 
will not be subject to the time it takes to communicate each message 
to Flowdock unless you are using Poor Man's cron.  The Drupal queue is 
run during cron execution so depending on how often you would like your 
log messages sent to Flowdock you may have to adjust how often your 
cron job is run.
